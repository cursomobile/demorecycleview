package com.example.demorecycleview;

public class MyListData {
    private String pos;
    private String description;
    private int imgId;

    public MyListData (String pos, String description, int imgId) {
        this.pos = pos;
        this.description = description;
        this.imgId = imgId;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}
