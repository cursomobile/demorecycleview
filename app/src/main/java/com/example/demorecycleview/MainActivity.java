package com.example.demorecycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyListData[] myListData = new MyListData[]{
                new MyListData("1","Email", android.R.drawable.ic_dialog_email),
                new MyListData("2","Info", android.R.drawable.ic_dialog_info),
                new MyListData("3","Delete", android.R.drawable.ic_delete),
                new MyListData("4","Dialer", android.R.drawable.ic_dialog_dialer),
                new MyListData("5","Alert", android.R.drawable.ic_dialog_alert),
                new MyListData("6","Map", android.R.drawable.ic_dialog_map),
                new MyListData("7","Email", android.R.drawable.ic_dialog_email),
                new MyListData("8","Info", android.R.drawable.ic_dialog_info),
                new MyListData("9","Delete", android.R.drawable.ic_delete),
                new MyListData("10","Dialer", android.R.drawable.ic_dialog_dialer),
                new MyListData("11","Alert", android.R.drawable.ic_dialog_alert),
                new MyListData("12","Map", android.R.drawable.ic_dialog_map),
        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        MyListAdapter adapter = new MyListAdapter(myListData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }
}
